package com.GreatLearning.User;

import com.GreatLearning.SaurabiAssignment.Login;
import com.GreatLearning.SaurabiAssignment.loginThread;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

public class CustomerMain implements Login {

	private int id;
	private String userName;
	private String password;
	
	HashMap<Integer, Integer> map;
	Statement stmt;
	Scanner scan = new Scanner(System.in);
	
	//Constructor for the class which will hold the MYSQL connection with
	//it's object for all the functionalities.
	public CustomerMain(Statement stmt) {
		this.stmt = stmt;
	}
	//Overriding the Login interface methods as per the Customer in runtime.
	@Override
	public void login(Login user) throws SQLException {
		loginThread th1 = new loginThread(stmt,user);
		th1.start();
	}

	@Override
	public void startSession(Login user) throws SQLException {
		takeOrder(user);
	}
	//After successfull Admin login, it will be responsible for attending the Customer.
	public void takeOrder(Login user) throws SQLException {
		map = foodMenu.showMenu(stmt);
		System.out.println("\nWould you like to make an order sir ? Y/N ");
		char ch = scan.next().charAt(0);
		completeOrder(user,ch);
	}
	
	@Override
	public void logout() throws SQLException {
		stmt.close();
	}
	//It will check the user input and calculate the total foodbill of the customer.
	public void completeOrder(Login user,char ch) throws SQLException {
		if (ch == 'N') {
			System.out.println("Thank you for ordering with us, hope you visit soon!");
		}else if(ch == 'Y') {
		System.out.println("How many foodItems you would like to have sir ?");
		int orderCount = scan.nextInt();
		System.out.println("Please input the requied foodItem Numbers from the given Menu.");
		int bill = 0;
		while (--orderCount >= 0) {
			int order = scan.nextInt();
			//Getting all the price list from the HashMap created while the showing the FoodMenu table to Customer.
			if (!map.containsKey(order)) {
				System.out.println("Specified ItemNumber doesn't exist in foodMenu, please try again!\n");
				completeOrder(user,ch);
				return ;
			}
			else bill += map.get(order);
		}
		System.out.println("\nTotal bill for your order is = " + bill);
		System.out.println();
		updateUserBill(bill,user);
		orderAgain(user);
		}else {
			System.out.println("Wrong input, try again");
			System.out.println("\nWould you like to make an order sir ? Y/N ");
			char cc = scan.next().charAt(0);
			completeOrder(user,cc);
			return ;
		}
	}

	private void orderAgain(Login user) throws SQLException {
		System.out.println("Would you like to order again ? Y/N");
		char ch = scan.next().charAt(0);
		completeOrder(user, ch);
	}
	//Updating the calculated FoodBill into the Database table named as userbills
	//For admin usage.
	public void updateUserBill(int bill,Login user) throws SQLException {
		String date = java.time.LocalDate.now().toString();
		//System.out.println("Inserting UserBill in database");
		String str = "INSERT INTO `surabi`.`userbills` (`Date`, `UserName`, `Bill`) VALUES ('" + date + "','" 
		+ user.getUserName() + "','" + bill + "')";
		//Writing a update querry in sql, which will update the generated bill along with username and date.
		stmt.executeUpdate(str);
	}

	@Override
	public void setId(int id) {
		this.id = id ;
	}

	@Override
	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Override
	public void setUserPassword(String password) {
		this.password = password;
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public String getUserName() {
		return userName;
	}

	@Override
	public String getUserPassword() {
		return password;
	}
	
}
