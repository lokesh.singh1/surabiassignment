package com.GreatLearning.User;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.Scanner;

import com.GreatLearning.SaurabiAssignment.Login;
import com.GreatLearning.SaurabiAssignment.loginThread;
import com.mysql.cj.jdbc.result.ResultSetMetaData;

public class AdminMain implements Login {

	private int id ;
	private String userName ;
	private String password ;
	Statement stmt;
	Scanner scan = new Scanner(System.in);

	//Constructor for the class which will hold the MYSQL connection with
	//it's object for all the functionalities.
	public AdminMain(Statement stmt) {
		this.stmt = stmt;
	}
	//Overriding the Login interface methods as per the Admin in runtime.
	@Override
	public void login(Login user) throws SQLException {
		loginThread th1 = new loginThread(stmt, user);
		th1.start();
	}

	@Override
	public void startSession(Login user) throws SQLException {
		System.out.println("Welcome to administration department!\n");
		postAdminLoggedIn(stmt,user);
	}
	
	@Override
	public void logout() throws SQLException {
		stmt.close();
	}
	//After successfull Admin login, it will be responsible for attending the admin.
	public void postAdminLoggedIn(Statement stmt,Login users) throws SQLException {
		// TODO Auto-generated method stub
		System.out.println("Choose one option sir:");
		System.out.println("1:- All bills generated today ? \n2:- Total sale for this month");
		int option = scan.nextInt();
		System.out.println();
		switch (option) {
		case 1: {
			System.out.println("Showing all bills generated today");
			LocalDate date = java.time.LocalDate.now();
			//LocalDate variable stores the current date as per the system date.
			String str = "Select * from userbills where DAY(Date) = '" + date.getDayOfMonth() + "'";
			//SQL querry for selecting all the entries in userbills table having Today's date.
			ResultSet rset = stmt.executeQuery(str);
			ResultSetMetaData rsmd = (ResultSetMetaData) rset.getMetaData();
			//ResultSetMetaDeta will get the Table structure from the database.
			int columnsNumber = rsmd.getColumnCount();
			while (rset.next()) {
				for (int i = 1; i <= columnsNumber; i++) {
					if (i > 1)
						System.out.print(",  ");
					String columnValue = rset.getString(i);
					System.out.print(columnValue + "  " + rsmd.getColumnName(i));
				}
				System.out.println("");
			}
		}
			break;
		case 2: {
			LocalDate date = java.time.LocalDate.now();
			String str = "Select * from userbills where MONTH(Date) = '" + date.getMonthValue() + "'";
			//SQL querry for selecting all the entries in userbills table having month as current month.
			ResultSet rset = stmt.executeQuery(str);
			ResultSetMetaData rsmd = (ResultSetMetaData) rset.getMetaData();
			int columnsNumber = rsmd.getColumnCount();
			int ans = 0;
			while (rset.next()) {
				ans += Integer.valueOf(rset.getString(columnsNumber));
			}
			System.out.println("Total sale for this month = " + ans);
		}
			break;
		default: {
			System.out.println("Wrong input detected, please choose a valid option again! \n");
			postAdminLoggedIn(stmt,users);
			return ;
			}
		}
		continueOrLogout(stmt,users);
	}

	public void continueOrLogout(Statement stmt2, Login users) throws SQLException {
		//We will ask Admin if he want to run same or more querry or not ?
		System.out.println("\nplease choose an option");
		System.out.println("1:- Continue \n2:- logout");
		int in = scan.nextInt();
		switch(in) {
			case 1 : postAdminLoggedIn(stmt, users);
			break ;
			case 2 : { System.out.println("Successfully Logged Out");
						logout();
					}
			break ;
			default : {
				System.out.println("Wrong input, please try again");
				continueOrLogout(stmt2, users);
				return ;
			}
		}
	}

	//Overriding function as per Admin Details.
	@Override
	public void setId(int id) {
		this.id = id ;
	}

	@Override
	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Override
	public void setUserPassword(String password) {
		this.password = password;
	}

	@Override
	public int getId() {
		return id ;
	}

	@Override
	public String getUserName() {
		return userName ;
	}

	@Override
	public String getUserPassword() {
		return password;
	}

}
