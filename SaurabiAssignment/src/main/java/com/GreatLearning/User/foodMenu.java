package com.GreatLearning.User;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

import com.mysql.cj.jdbc.result.ResultSetMetaData;

public class foodMenu {
	
	//It will display the foodMenu table from the databse to the Customer along with the column names
	//from the table foodmenu.
	public static HashMap<Integer, Integer> showMenu(Statement stmt) throws SQLException {
		System.out.println("Showing foodMenu to User");
		String str = "select * from foodmenu";
		ResultSet rset = stmt.executeQuery(str);
		//ResultSetMetaDeta will get the Table structure from the database.
		ResultSetMetaData rsmd = (ResultSetMetaData) rset.getMetaData();
		int columnsNumber = rsmd.getColumnCount();
		HashMap<Integer, Integer> map = new HashMap<>();
		//We will be storing the foodItem number and it's price as KEY-Value in HashMap
		//and returning it back to the CustomerMain object for calaculating the Bill.
		while (rset.next()) {
			int temp = 0;
			for (int i = 1; i <= columnsNumber; i++) {
				if (i > 1)
					System.out.print(",  ");
				String columnValue = rset.getString(i);
				if (i == 1) {
					temp = Integer.valueOf(columnValue);
				}
				System.out.print(columnValue + " " + rsmd.getColumnName(i));
				if (i == columnsNumber) {
					map.put(temp, Integer.valueOf(columnValue));
				}
			}
			System.out.println("");
		}
		return map;
	}

}
