package com.GreatLearning.SaurabiAssignment;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import com.GreatLearning.User.AuntheticatorDAO;

public class loginThread extends Thread {
	Scanner scan = new Scanner(System.in);

	public loginThread(Statement stmt, Login user) throws SQLException {
		//Created a loginThread which will be common for both the users
		//admin as well as customer.
		System.out.println("\nPlease enter the User Datails");
		System.out.println("userId");
		int id = scan.nextInt();
		System.out.println("userName:- ");
		String userName = scan.next();
		System.out.println("Password:- ");
		String password = scan.next();
		user.setId(id);
		user.setUserName(userName);
		user.setUserPassword(password);
		//Creating a Dao class object which will be responsible
		//for checking the credentials from the databse.
		AuntheticatorDAO userdao = new AuntheticatorDAO();
		boolean isValidAttempt = userdao.login(stmt, user);
		if (isValidAttempt) {
			System.out.println("Successfull login\n");
			//After successful login, user will start its functionality
			user.startSession(user);
		} else {
			System.out.println("Invalid attempt Try again");
			return;
		}
	}

}
