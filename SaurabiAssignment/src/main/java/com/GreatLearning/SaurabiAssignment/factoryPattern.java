package com.GreatLearning.SaurabiAssignment;

import java.sql.Statement;

import com.GreatLearning.User.AdminMain;
import com.GreatLearning.User.CustomerMain;

public class factoryPattern {

	public static Login getUserType(int option, Statement stmt) {
		// It will return the class object as per user input between admin/customer
		//in the runtime.
		if (option == 1) {
			return new CustomerMain(stmt);
		} else if (option == 2) {
			return new AdminMain(stmt);
		} else {
			return null;
		}
	}
}
