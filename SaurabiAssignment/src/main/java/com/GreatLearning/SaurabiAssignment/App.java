package com.GreatLearning.SaurabiAssignment;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import com.GreatLearning.Connection.MySQLConnection;

public class App {
	public static void main(String[] args) throws SQLException {
		//Creating MysqlConnection using Singleton Design Pattern
		MySQLConnection myConnection = MySQLConnection.getConnection();
		Statement stmt = myConnection.conn.createStatement();
		System.out.println("Please Choose an option");
		System.out.println("1:- Customer");
		System.out.println("2:- Admin");
		Scanner scan = new Scanner(System.in);
		int mainOption = scan.nextInt();
		switch (mainOption) {
		case 1: {
			//Creating customer/admin class object at run time
			//with use of Factory Design Pattern
			Login user = factoryPattern.getUserType(mainOption, stmt);
			user.login(user);
			break;
		}
		case 2: {
			//Creating customer/admin class object at run time
			//with use of Factory Design Pattern
			Login user = factoryPattern.getUserType(mainOption, stmt);
			user.login(user);
			break;
		}
		default:
			System.out.println("Wrong input!");
		}
		scan.close();
	}
}
