package com.GreatLearning.SaurabiAssignment;

import java.sql.SQLException;

public interface Login {
	
	//Making an interface having all the important and common
	//functionality between Admin/Customer.

	public void setId(int id);
	
	public void setUserName(String userName);
	
	public void setUserPassword(String password);
	
	public int getId();
	
	public String getUserName();
	
	public String getUserPassword();

	public void login(Login user) throws SQLException;
	
	public void logout() throws SQLException;

	public void startSession(Login users) throws SQLException;
}
