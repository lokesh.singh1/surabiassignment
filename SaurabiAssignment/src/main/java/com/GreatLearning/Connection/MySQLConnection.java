package com.GreatLearning.Connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MySQLConnection {
	private static MySQLConnection single_connection = null;
	public Connection conn;

	private MySQLConnection() throws SQLException {
		conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/surabi", "root", "mysql27397");
	}
	// Using Singleton Design Pattern as only one connection will be allowed at a
	// time.
	static public MySQLConnection getConnection() throws SQLException {
		if (single_connection == null) {
			single_connection = new MySQLConnection();
		}
		return single_connection;

	}
}
