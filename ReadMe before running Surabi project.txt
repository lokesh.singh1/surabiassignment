-> Please check the SQL Connection driver before running the application.

-> Run Surabi.sql script given in this folder to create the database. 
  {Three tables will get created using this sql script named userbills, users and fooditems in "surabi" database} 

-> Run Maven project present in the given repository namely SurabiAssignment using eclipse/IDE.

#kEY_POINTS

1.In this application, I've categorized users into customer and admin.
2. All the user creation in done using SQL querries as given in the .sql script.
3. One of existing user that will be created after running .sql script will be id: "101", name: "loki", pass: "123"
4. An admin user can also be a customer of our application and hence can logged in using his/her username and password.
5. You can add other users by running this query :
   INSERT INTO `surabi`.`users` (`id`, `userName`, `password`) VALUES ('int', 'String', 'String');

#STEPS_TO_RUN

1. Input whether user want to access "admin" or "customer" functionalities as "1" or "2" respectively.
2. Based upon the choice, user have to provide username and password for aunthentication. 
3. After successful login, user will get further information according to their role.
4. Customer will be asked to place an order while admin will get options to see the sale daywise/monthwise.
5. User will get logged out of the system automatically by inputing "N" in the end of programm.

#DESIGN_PATTERS_USED

1. Singleton: For creating databse connection thread safe, singleton design pattern is used.
2. Factory: Factory design pattern is used to get the controllers based on user(Customer/Admin) at runtime.
	    It allows us to hide business logic (DAO classes) from the client.


